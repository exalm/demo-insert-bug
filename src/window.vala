/* window.vala
 *
 * Copyright 2020 Alexander Mikhaylenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[GtkTemplate (ui = "/org/example/App/window.ui")]
public class Demo.Window : Gtk.ApplicationWindow {
    [GtkChild]
    private Gtk.Label child1;
    [GtkChild]
    private Gtk.Label child2;
    [GtkChild]
    private Hdy.Deck deck1;
    [GtkChild]
    private Hdy.Deck deck2;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    [GtkCallback]
    private void clicked_cb () {
        deck1.insert_child_after (new Gtk.Label ("New child") {
            visible = true
        }, child1);
        deck2.insert_child_after (new Gtk.Label ("New child") {
            visible = true
        }, child2);
    }
}
